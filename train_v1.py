from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import joblib

import pandas as pd

import argparse

parser = argparse.ArgumentParser(description='Train a KNN classifier.')
parser.add_argument('--data_path', type=str, help='Path to data')
parser.add_argument('--output_path', type=str, help='Output data')

args = parser.parse_args()


data = pd.read_csv(args.data_path)

X = data.drop(columns=['health'])

y = data['health']





# Разделение данных на обучающий и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Создание и обучение модели
clf = KNeighborsClassifier(n_neighbors=5)  # Используем 5 ближайших соседей
clf.fit(X_train, y_train)

# Прогнозирование на тестовом наборе
y_pred = clf.predict(X_test)

# Оценка качества модели
accuracy = accuracy_score(y_test, y_pred)
print("KNeighborsClassifier model/Accuracy:", accuracy)

joblib.dump(clf, args.output_path)