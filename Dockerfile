FROM continuumio/miniconda3

WORKDIR /app

COPY dev_environment.yml /app/environment.yml

RUN conda env create -f environment.yml

ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "hw_mlops"]