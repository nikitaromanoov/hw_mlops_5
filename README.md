# HW №1
Для обеспечения качества кода мы используем pre-commit хуки. Убедитесь, что вы установили все зависимости из `requirements.txt` и запустили `pre-commit install` перед началом работы.
# HW №2
Собрать докер:
`docker build -t mlops .`

Запуск:
`docker run mlops`

# HW "Snakemake"


This pipeline processes data for machine learning tasks using Snakemake. It includes steps for data retrieval, preprocessing, and model training.

## Steps
1. **Data Retrieval:** Download data from a public source.
2. **Preprocessing:** Clean and prepare data.
3. **Model Training:** Train a machine learning model.

To view the DAG of the workflow, use the following command:
```bash
snakemake --dag | dot -Tpng > dag.png
```
![Picture should be here](dag.png)


You can view the steps in CI/CD or here:

```bash
$ snakemake --cores 4 --latency-wait 600
Assuming unrestricted shared filesystem usage.
Building DAG of jobs...
Using shell: /usr/bin/bash
Provided cores: 4
Rules claiming more threads will be scaled down.
Job stats:
job                count
---------------  -------
all                    1
preprocess_data        2
read_data              1
train_model            4
total                  8
Select jobs to execute...
Execute 1 jobs...
[Thu Apr 25 20:53:57 2024]
localrule read_data:
    output: data.csv
    jobid: 3
    reason: Missing output files: data.csv
    resources: tmpdir=/tmp
Dataset URL: https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data
[Thu Apr 25 20:54:12 2024]
Finished job 3.
1 of 8 steps (12%) done
Select jobs to execute...
Execute 2 jobs...
[Thu Apr 25 20:54:12 2024]
localrule preprocess_data:
    input: data.csv
    output: data_after_preprocess_v2.csv
    jobid: 5
    reason: Missing output files: data_after_preprocess_v2.csv; Input files updated by another job: data.csv
    wildcards: preprocess=v2
    resources: tmpdir=/tmp
[Thu Apr 25 20:54:12 2024]
localrule preprocess_data:
    input: data.csv
    output: data_after_preprocess_v1.csv
    jobid: 2
    reason: Missing output files: data_after_preprocess_v1.csv; Input files updated by another job: data.csv
    wildcards: preprocess=v1
    resources: tmpdir=/tmp
[Thu Apr 25 20:54:18 2024]
Finished job 2.
2 of 8 steps (25%) done
Select jobs to execute...
[Thu Apr 25 20:54:18 2024]
Finished job 5.
3 of 8 steps (38%) done
Execute 1 jobs...
[Thu Apr 25 20:54:18 2024]
localrule train_model:
    input: data_after_preprocess_v2.csv
    output: model_v1_v2.pkl
    jobid: 4
    reason: Missing output files: model_v1_v2.pkl; Input files updated by another job: data_after_preprocess_v2.csv
    wildcards: version=v1, preprocess=v2
    threads: 4
    resources: tmpdir=/tmp
KNeighborsClassifier model/Accuracy: 0.6961821527138914
[Thu Apr 25 20:54:20 2024]
Finished job 4.
4 of 8 steps (50%) done
Select jobs to execute...
Execute 1 jobs...
[Thu Apr 25 20:54:20 2024]
localrule train_model:
    input: data_after_preprocess_v2.csv
    output: model_v2_v2.pkl
    jobid: 7
    reason: Missing output files: model_v2_v2.pkl; Input files updated by another job: data_after_preprocess_v2.csv
    wildcards: version=v2, preprocess=v2
    threads: 4
    resources: tmpdir=/tmp
/usr/local/lib/python3.11/site-packages/sklearn/svm/_classes.py:31: FutureWarning: The default value of `dual` will change from `True` to `'auto'` in 1.5. Set the value of `dual` explicitly to suppress the warning.
  warnings.warn(
/usr/local/lib/python3.11/site-packages/sklearn/svm/_base.py:1237: ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.
  warnings.warn(
 Linear model/Accuracy: 0.7141214351425943
[Thu Apr 25 20:54:26 2024]
Finished job 7.
5 of 8 steps (62%) done
Select jobs to execute...
Execute 1 jobs...
[Thu Apr 25 20:54:26 2024]
localrule train_model:
    input: data_after_preprocess_v1.csv
    output: model_v1_v1.pkl
    jobid: 1
    reason: Missing output files: model_v1_v1.pkl; Input files updated by another job: data_after_preprocess_v1.csv
    wildcards: version=v1, preprocess=v1
    threads: 4
    resources: tmpdir=/tmp
KNeighborsClassifier model/Accuracy: 0.6961821527138914
[Thu Apr 25 20:54:27 2024]
Finished job 1.
6 of 8 steps (75%) done
Select jobs to execute...
Execute 1 jobs...
[Thu Apr 25 20:54:27 2024]
localrule train_model:
    input: data_after_preprocess_v1.csv
    output: model_v2_v1.pkl
    jobid: 6
    reason: Missing output files: model_v2_v1.pkl; Input files updated by another job: data_after_preprocess_v1.csv
    wildcards: version=v2, preprocess=v1
    threads: 4
    resources: tmpdir=/tmp
/usr/local/lib/python3.11/site-packages/sklearn/svm/_classes.py:31: FutureWarning: The default value of `dual` will change from `True` to `'auto'` in 1.5. Set the value of `dual` explicitly to suppress the warning.
  warnings.warn(
/usr/local/lib/python3.11/site-packages/sklearn/svm/_base.py:1237: ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.
  warnings.warn(
 Linear model/Accuracy: 0.6437442502299908
[Thu Apr 25 20:54:32 2024]
Finished job 6.
7 of 8 steps (88%) done
Select jobs to execute...
Execute 1 jobs...
[Thu Apr 25 20:54:32 2024]
localrule all:
    input: model_v1_v1.pkl, model_v1_v2.pkl, model_v2_v1.pkl, model_v2_v2.pkl
    jobid: 0
    reason: Input files updated by another job: model_v1_v1.pkl, model_v2_v2.pkl, model_v2_v1.pkl, model_v1_v2.pkl
    resources: tmpdir=/tmp
[Thu Apr 25 20:54:32 2024]
Finished job 0.
8 of 8 steps (100%) done
Complete log: .snakemake/log/2024-04-25T205357.905229.snakemake.log
```