import os
import json
import pandas as pd

os.mkdir(os.path.expanduser("/root/.kaggle/"))

kaggle_json_file = "/root/.kaggle/kaggle.json"

if not os.path.exists(kaggle_json_file):
    open(kaggle_json_file, 'a').close() 


api_token = {"username":"username","key":"api-key"}


with open("/root/.kaggle/kaggle.json", 'w') as file:
    json.dump(api_token, file)

os.chmod("/root/.kaggle/kaggle.json", 0o600)

import kaggle as kg


kg.api.dataset_download_files(dataset = "new-york-city/ny-2015-street-tree-census-tree-data", path='data', unzip=True) 

df = pd.read_csv("./data/2015-street-tree-census-tree-data.csv") 

df.to_csv("data.csv")