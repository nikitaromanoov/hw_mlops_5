from sklearn.preprocessing import LabelEncoder
import pandas as pd 

import argparse

parser = argparse.ArgumentParser(description='Train a KNN classifier.')
parser.add_argument('--data_path', type=str, help='Path to data')
args = parser.parse_args()

X =  pd.read_csv(args.data_path)


X = X.dropna()
X = X.drop(columns=["created_at","status", 'steward',
       'guards',  'address', 'postcode', 'zip_city', "problems", "state"]) 


label_encoder = LabelEncoder()
X['spc_latin'] = label_encoder.fit_transform(X['spc_latin'])
X['spc_common'] = label_encoder.fit_transform(X['spc_common'])
X['curb_loc'] = label_encoder.fit_transform(X['curb_loc'])
X['spc_common'] = label_encoder.fit_transform(X['spc_common'])
X['sidewalk'] = label_encoder.fit_transform(X['sidewalk'])
X['user_type'] = label_encoder.fit_transform(X['user_type'])

X['root_stone'] = label_encoder.fit_transform(X['root_stone'])
X['root_grate'] = label_encoder.fit_transform(X['root_grate'])
X['root_other'] = label_encoder.fit_transform(X['root_other'])
X['trunk_wire'] = label_encoder.fit_transform(X['trunk_wire'])
X['trnk_light'] = label_encoder.fit_transform(X['trnk_light'])
X['trnk_other'] = label_encoder.fit_transform(X['trnk_other'])
X['brch_light'] = label_encoder.fit_transform(X['brch_light'])
X['brch_shoe'] = label_encoder.fit_transform(X['brch_shoe'])
X['brch_other'] = label_encoder.fit_transform(X['brch_other'])


X['borocode'] = label_encoder.fit_transform(X['borocode'])

X['borough'] = label_encoder.fit_transform(X['borough'])
X['nta'] = label_encoder.fit_transform(X['nta'])
X['nta_name'] = label_encoder.fit_transform(X['nta_name'])


X.to_csv("data_after_preprocess_v2.csv")
