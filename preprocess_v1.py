from sklearn.preprocessing import LabelEncoder
import pandas as pd 

import argparse


parser = argparse.ArgumentParser(description='Train a KNN classifier.')
parser.add_argument('--data_path', type=str, help='Path to data')
args = parser.parse_args()

X = pd.read_csv(args.data_path)


X = X.dropna()
X= X.drop(columns=["created_at","status", 'steward',
       'guards', 'sidewalk', 'user_type', 'problems', 'root_stone',
       'root_grate', 'root_other', 'trunk_wire', 'trnk_light', 'trnk_other',
       'brch_light', 'brch_shoe', 'brch_other', 'address', 'postcode',
       'zip_city', 'community board', 'borocode', 'borough', 'cncldist',
       'st_assem', 'st_senate', 'nta', 'nta_name',])


label_encoder = LabelEncoder()
X['spc_latin'] = label_encoder.fit_transform(X['spc_latin'])
X['spc_common'] = label_encoder.fit_transform(X['spc_common'])
X['curb_loc'] = label_encoder.fit_transform(X['curb_loc'])
X['spc_common'] = label_encoder.fit_transform(X['spc_common'])
X['state'] = label_encoder.fit_transform(X['state'])

X.to_csv("data_after_preprocess_v1.csv")