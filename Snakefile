from hydra import compose, initialize
from hydra.core.config_store import ConfigStore
from omegaconf import OmegaConf
from hydra.utils import instantiate


import os

class MyModel:
    def __init__(self, script, resources):
        self.script = script
        self.resources = resources



config_path = "./"

initialize(config_path=config_path, job_name="config")

cfg_preprocessing = compose(config_name="preprocessing_config.yaml")

cfg_training = compose(config_name="model_config.yaml")

os.environ["NUM_CORES"] = str(int(os.cpu_count()/2))

model = instantiate(cfg_training.v1)


rule all:
    input:
        expand("model_{version}_{preprocess}.pkl", version=["v1", "v2"], preprocess=["v1", "v2"])


rule read_data:
    output:
        "data.csv"
    shell:
        "python read_data.py"

rule preprocess_data:
    input:
        "data.csv"
    output:
        "data_after_preprocess_{preprocess}.csv"
    params:
        preprocess_script=lambda wildcards: cfg_preprocessing[wildcards.preprocess]["script"]
    shell:
        "python {params.preprocess_script} --data_path {input}"

rule train_model:
    input:
        preprocess_output="data_after_preprocess_{preprocess}.csv"
    output:
        "model_{version}_{preprocess}.pkl"
    params:
        training_script = lambda wildcards: cfg_training[wildcards.version]["script"]
    threads: int(os.environ["NUM_CORES"])
    shell:
        "python {params.training_script} --data_path {input.preprocess_output} --output_path {output}"
